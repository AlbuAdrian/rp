from django.apps import AppConfig


class EquipmentsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'equipments'

    # def ready(self):
    #     from jobs import jobs
    #     from apscheduler.schedulers.background import BackgroundScheduler
    #     scheduler = BackgroundScheduler()
    #     print("adding job....")
    #     scheduler.add_job(jobs.send_email, 'interval', minutes=5, id='my_job_id')
    #     scheduler.start()
