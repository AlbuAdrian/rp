from django import forms
from django.forms import TextInput, Select
from equipments.models import Equipment


class EquipmentsForm(forms.ModelForm):
    class Meta:
        model = Equipment

        fields = '__all__'
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Denumirea echipamentului', 'class': 'form-control'}),
            'pos': Select(attrs={'class': 'form-control'}),
            'serial_number': TextInput(
                attrs={'placeholder': 'Please enter serial number if exists', 'class': 'form-control'}),
            'inventory_number': TextInput(
                attrs={'placeholder': 'Please enter inventory number if exists', 'class': 'form-control'}),
            'maintenances': forms.CheckboxSelectMultiple()
        }

    def __init__(self, *args, **kwargs):
        super(EquipmentsForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Denumirea Echipamentului'
        self.fields['pos'].label = 'Locatia unde este instalat'
