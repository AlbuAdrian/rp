# Generated by Django 4.0.3 on 2022-05-04 17:21

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('equipments', '0002_maintenancedone_user_value_input'),
    ]

    operations = [
        migrations.AddField(
            model_name='maintenancedone',
            name='due_date',
            field=models.DateField(default=django.utils.timezone.now),
        ),
    ]
