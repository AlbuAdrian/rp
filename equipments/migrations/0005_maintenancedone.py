# Generated by Django 4.0.3 on 2022-05-05 18:03

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('maintenance', '0001_initial'),
        ('equipments', '0004_delete_maintenancedone'),
    ]

    operations = [
        migrations.CreateModel(
            name='MaintenanceDone',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_done', models.DateField(default=django.utils.timezone.now)),
                ('due_date', models.DateField(default=django.utils.timezone.now)),
                ('user_value_input', models.IntegerField(default=0)),
                ('equipment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='equipments.equipment')),
                ('maintenance', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='maintenance.maintenance')),
            ],
        ),
    ]
