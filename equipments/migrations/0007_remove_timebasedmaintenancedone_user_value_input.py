# Generated by Django 4.0.3 on 2022-05-10 17:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipments', '0006_rename_maintenancedone_timebasedmaintenancedone_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='timebasedmaintenancedone',
            name='user_value_input',
        ),
    ]
