from django.db import models
from django.utils import timezone
import maintenance.models
from pos.models import Pos


class Equipment(models.Model):
    name = models.CharField(max_length=30)
    pos = models.ForeignKey(Pos, null=True, blank=True, on_delete=models.SET_NULL)
    serial_number = models.CharField(max_length=30)
    inventory_number = models.CharField(max_length=30)
    maintenances = models.ManyToManyField(maintenance.models.Maintenance)
    date_added = models.DateField(default=timezone.now)

    def __str__(self):
        return f'{self.name}'


class TimeBasedMaintenanceDone(models.Model):
    equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE)
    maintenance = models.ForeignKey(maintenance.models.Maintenance, on_delete=models.CASCADE)
    date_done = models.DateField(default=timezone.now)
    due_date = models.DateField()


class ValueBasedMaintenanceDone(models.Model):
    equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE)
    maintenance = models.ForeignKey(maintenance.models.Maintenance, on_delete=models.CASCADE)
    input_value = models.IntegerField(blank=False, null=False, default=0)
    due_value = models.IntegerField(blank=False, null=False, default=0)
    user_value_input = models.IntegerField(null=False, blank=False, default=0)