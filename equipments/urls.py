from django.urls import path

from equipments import views

urlpatterns = [
    path('add-equipment/', views.EquipmentsCreateView.as_view(), name='add_equipment'),
    path('list-of-equipments/', views.EquipmentsListView.as_view(), name='list_of_equipments'),
    path('update-equipment/<int:pk>/', views.EquipmentsUpdateView.as_view(), name='update_equipment'),
    path('delete-equipment/<int:pk>/', views.EquipmentsDeleteView.as_view(), name='delete_equipment'),
    path('details-equipment/<int:pk>/', views.EquipmentsDetailsView.as_view(), name='equipment_details'),

]
