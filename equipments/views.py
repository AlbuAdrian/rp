from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView

from equipments.forms import EquipmentsForm
from equipments.models import Equipment


class EquipmentsCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    template_name = 'equipments/add_new_equipment.html'
    model = Equipment
    form_class = EquipmentsForm
    success_url = reverse_lazy('list_of_equipments')

    def test_func(self):
        return self.request.user.is_manager(), self.request.user.is_superuser()


class EquipmentsListView(ListView):
    template_name = 'equipments/list_of_equipments.html'
    model = Equipment
    context_object_name = 'all_equipments'


class EquipmentsUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    template_name = 'equipments/update_equipment.html'
    model = Equipment
    form_class = EquipmentsForm
    success_url = reverse_lazy('list_of_equipments')

    def test_func(self):
        return self.request.user.is_manager(), self.request.user.is_superuser()


class EquipmentsDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    template_name = 'equipments/delete_equipment.html'
    model = Equipment
    success_url = reverse_lazy('list_of_equipments')

    def test_func(self):
        return self.request.user.is_manager(), self.request.user.is_superuser()


class EquipmentsDetailsView(LoginRequiredMixin, DetailView):
    template_name = 'equipments/detail_view_equipment.html'
    model = Equipment
