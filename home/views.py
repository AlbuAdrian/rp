from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView


# def index(request):
#     return HttpResponse('Hello World!')
from equipments.models import Equipment


class HomeTemplateView(TemplateView):
    template_name = 'home/home.html'

    def get_context_data(self, **kwargs):
        return {'all_equipments': Equipment.objects.all()}

