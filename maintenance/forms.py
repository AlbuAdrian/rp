from django import forms
from django.forms import TextInput, Textarea

from maintenance.models import Maintenance, recurrence_types, conditions


class ConditionsForm(forms.ModelForm):
    class Meta:
        model = Maintenance
        fields = '__all__'
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Adauga o denumire pt. noua conditie', 'class': 'form-control'}),
            'type': forms.Select(choices=recurrence_types),
            'recurrence': TextInput(
                attrs={'placeholder': 'Completeaza cu numar de zile sau valoare ', 'class': 'form-control'}),
            'condition': forms.Select(choices=conditions, attrs={'placeholder': 'selecteaza conditia valorii adaugate'})
        }

    def __init__(self, *args, **kwargs):
        super(ConditionsForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Numele Conditiei'
        self.fields['type'].label = 'Tipul Conditiei'
        self.fields['recurrence'].label = 'Recurenta'
        self.fields['condition'].label = 'Conditia valorii'

    def clean(self):
        cleaned_data = self.cleaned_data
        name_input = cleaned_data.get('name')
        all_conditions = Maintenance.objects.all()
        for category in all_conditions:
            if name_input == category.name:
                msg = 'This name of condition already exist!!'
                self._errors['name'] = self.error_class([msg])
        return cleaned_data
