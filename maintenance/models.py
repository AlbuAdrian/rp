from django.db import models

recurrence_types = [('TIME-BASED', 'time-based'), ('VALUE-BASED', 'value-based')]
conditions = [('SMALLER_THAN', 'mai mica decat'), ('GRATER-THAN', 'mai mare decat')]


class Maintenance(models.Model):
    name = models.CharField(max_length=30, null=False, blank=False)
    type = models.CharField(max_length=30, choices=recurrence_types)
    recurrence = models.IntegerField()
    condition = models.CharField(max_length=30, choices=conditions, null=True, blank=True)

    def __str__(self):
        return f'{self.name}'



