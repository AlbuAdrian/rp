from django.urls import path

from maintenance import views

urlpatterns = [
    path('create-maintenance/', views.ConditionsCreateView.as_view(), name='create_condition'),
    path('list-of-maintenance/', views.ConditionsListView.as_view(), name='list_of_conditions'),
    path('update-maintenance/<int:pk>/', views.ConditionsUpdateView.as_view(), name='update_condition'),
    path('delete-condition/<int:pk>/', views.ConditionsDeleteView.as_view(), name='delete_condition')
]
