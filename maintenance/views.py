from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin, UserPassesTestMixin
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from maintenance.forms import ConditionsForm
from maintenance.models import Maintenance


class ConditionsCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    template_name = 'conditions/add_new_condition.html'
    model = Maintenance
    form_class = ConditionsForm
    success_url = reverse_lazy('list_of_conditions')

    def test_func(self):
        return self.request.user.is_manager(), self.request.user.is_superuser()


class ConditionsListView(ListView):
    template_name = 'conditions/list_of_conditions.html'
    model = Maintenance
    context_object_name = 'all_conditions'


class ConditionsUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    template_name = 'conditions/update_condition.html'
    model = Maintenance
    form_class = ConditionsForm
    success_url = reverse_lazy('list_of_conditions')

    def test_func(self):
        return self.request.user.is_manager(), self.request.user.is_superuser()


class ConditionsDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    template_name = 'conditions/delete_condition.html'
    model = Maintenance
    success_url = reverse_lazy('list_of_conditions')

    def test_func(self):
        return self.request.user.is_manager(), self.request.user.is_superuser()
