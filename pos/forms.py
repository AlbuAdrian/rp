from django import forms
from django.forms import TextInput, EmailInput

from pos.models import Pos


class PosForm(forms.ModelForm):
    class Meta:
        model = Pos

        fields = ['name', 'email']
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Please enter name', 'class': 'form-control'}),
            'email': EmailInput(attrs={'placeholder': 'Please enter email', 'class': 'form-control'})
        }

    def __init__(self, *args, **kwargs):
        super(PosForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Location Name'

    def clean(self):
        cleaned_data = self.cleaned_data
        name_input = cleaned_data.get('name')
        all_conditions = Pos.objects.all()
        for category in all_conditions:
            if name_input == category.name:
                msg = 'This name of Location already exist!!'
                self._errors['name'] = self.error_class([msg])
        return cleaned_data
