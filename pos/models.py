from django.db import models


class Pos(models.Model):
    name = models.CharField(max_length=30)
    email = models.CharField(max_length=70)


    def __str__(self):
        return f'{self.name}'
