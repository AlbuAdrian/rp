from django.urls import path

from pos import views

urlpatterns = [
    path('add-Pos/', views.PosCreateView.as_view(), name='add_pos'),
    path('list-of-Pos/', views.PosListView.as_view(), name='list_of_pos'),
    path('update-Pos/<int:pk>/', views.PosUpdateView.as_view(), name='update_pos'),
    path('delete-Pos/<int:pk>/', views.PosDeleteView.as_view(), name='delete_pos')
]
