from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin, UserPassesTestMixin
from django.shortcuts import render


# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from pos.forms import PosForm
from pos.models import Pos


class PosCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    template_name = 'pos/add_new_pos.html'
    model = Pos
    form_class = PosForm
    success_url = reverse_lazy('list_of_pos')
    # permission_required = 'pos.add_new_pos'

    def test_func(self):
        return self.request.user.is_manager(), self.request.user.is_superuser()


class PosListView(ListView):
    template_name = 'pos/list_of_pos.html'
    model = Pos
    context_object_name = 'all_pos'
    # permission_required = ''


class PosUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    template_name = 'pos/update_pos.html'
    model = Pos
    form_class = PosForm
    success_url = reverse_lazy('list_of_pos')
    # permission_required = 'pos.update_pos'

    def test_func(self):
        return self.request.user.is_manager(), self.request.user.is_superuser()


class PosDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    template_name = 'pos/delete_pos.html'
    model = Pos
    success_url = reverse_lazy('list_of_pos')
    # permission_required = 'pos.delete_pos'

    def test_func(self):
        return self.request.user.is_manager(), self.request.user.is_superuser()
