from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms import TextInput, EmailInput, NumberInput
from django.forms.widgets import Select

from userextend.models import UserExtend, user_status


class UserExtendForm(UserCreationForm):
    class Meta:
        model = UserExtend
        fields = ['first_name', 'last_name', 'phone_number', 'email', 'username', 'status', 'locatie']
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Completeaza numele', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Completeaza prenumele', 'class': 'form-control'}),
            'phone_number': NumberInput(
                attrs={'placeholder': 'Completeaza numarul de telefon', 'class': 'form-control'}),
            'email': EmailInput(attrs={'placeholder': 'Adauga email-ul', 'class': 'form-control'}),
            'username': TextInput(attrs={'placeholder': 'Stabileste un Username', 'class': 'form-control'}),
            'status': forms.Select(choices=user_status),
            'locatie': Select(attrs={'class': 'form-control'})
        }

    def __init__(self, *args, **kwargs):
        super(UserExtendForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password1'].widget.attrs['placeholder'] = 'Enter password'

        self.fields['password2'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['placeholder'] = 'Enter password confirmation'

    def clean(self):
        cleaned_data = self.cleaned_data
        status_input = cleaned_data.get('status')
        if status_input == 'manager':
            msg = 'You dont have permision'
            self._errors['name'] = self.error_class([msg])
        return cleaned_data
