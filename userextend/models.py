
from django.contrib.auth.models import User, AbstractUser
from django.db import models


from pos.models import Pos


user_status = [('LOCATIE', 'locatie'), ('SERVICE', 'service'), ('MANAGER', 'manager')]


class UserExtend(AbstractUser):
    phone_number = models.CharField(max_length=15, null=True, blank=True)
    status = models.CharField(max_length=10, choices=user_status, null=True, blank=True)
    locatie = models.ForeignKey(Pos, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.username

    def is_manager(self):
        return self.status == 'MANAGER'

    def is_superuser(self):
        return self.username == 'SuperUser'

    # def self_location(self):
    #     return self.locatie
