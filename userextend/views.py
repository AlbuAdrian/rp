from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import render

# Create your views here.
from django.contrib.auth.models import Group
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from userextend.forms import UserExtendForm
from userextend.models import UserExtend


class UserExtendCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    template_name = 'userextend/create_user.html'
    model = UserExtend
    form_class = UserExtendForm
    success_url = reverse_lazy('user_list')

    def test_func(self):
        return self.request.user.is_manager(), self.request.user.is_superuser()


class UserExtendListView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    template_name = 'userextend/user_list.html'
    model = UserExtend
    context_object_name = 'all_users'

    def test_func(self):
        return self.request.user.is_manager(), self.request.user.is_superuser()


class UserExtendUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    template_name = 'userextend/update_user.html'
    model = UserExtend
    form_class = UserExtendForm
    success_url = reverse_lazy('user_list')

    def test_func(self):
        return self.request.user.is_manager(), self.request.user.is_superuser()


class UserExtendDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    template_name = 'userextend/delete_user.html'
    model = UserExtend
    success_url = reverse_lazy('user_list')

    def test_func(self):
        return self.request.user.is_manager(), self.request.user.is_superuser()
