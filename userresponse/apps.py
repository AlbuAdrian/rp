from django.apps import AppConfig


class UserresponseConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'userresponse'
